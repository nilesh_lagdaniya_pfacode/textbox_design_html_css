document.getElementById('registerForm').addEventListener('submit', function(event){
    event.preventDefault()

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const contact = document.getElementById('contact').value;
    const message = document.getElementById('message').value;

    clearError()

    if(!name){
        showError('nameError', "Name is Required")
    }
    if(!email){
        showError('emailError', "Email is Required")
    }else if(!isValidEmail(email)){
        showError("emailError", "Invalid email Format")
    }
    if(!contact){
        showError("contactError", "Contact is Required")
    }else if(contact.length > 10){
        showError("contactError", "Please enter a valid contact")
    }
    if(!message){
        showError('messageError', "Message is Required")
    }


    function showError(elementId,errorMessage){
        const errorElement = document.getElementById(elementId)
        errorElement.innerHTML = errorMessage
    }
    function clearError(){
        const errors = document.querySelectorAll('.error')
        errors.forEach((error) => (error.innerText = ""))
    }
    function isValidEmail(email){
        const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        return emailRegex.test(email)
    }
})